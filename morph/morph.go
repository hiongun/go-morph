package morph

import (
    "fmt"
    "runtime"
    "os"
    "log"
    "bufio"
    "io"
    "strings"
    "sort"
)

type MInfo struct {
    level string  // G1 ~ G9, GA
    root string   // tolowered root for comparison
    prefix string // "-", "", "mal", "eks" // "-" means a link to this direction
    stem string // "", "amik", "ej", "ad"
    postfix string // "-", "o", "a", "n", "", "!" // "-" means a link to this direction
    org string // original dict entry, in duplicated cases, each concatenated like "org|org"
}

func (p *MInfo) Level() string {
    return p.level
}

func (p *MInfo) Org() string {
    return p.org
}

type Candidate struct {
    morphs []string
    weight int
}

func (c *Candidate) Morphs() []string {
    return c.morphs
}

func (c *Candidate) Weight() int {
    return c.weight
}


func (c Candidate) String() string {
    str := ""
    for i, morph := range(c.morphs) {
        str += morph
        if i < (len(c.morphs)-1) {
            str += " "
        }
    }
    str += ":" + fmt.Sprintf("%d", c.weight)
    return str
}

type ByWeight []Candidate

func (a ByWeight) Len() int { return len(a) }
func (a ByWeight) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a ByWeight) Less(i, j int) bool { return a[i].weight < a[j].weight }

var dict map[string]*MInfo = make(map[string]*MInfo)

func GetDict() map[string]*MInfo {
    return dict
}

var candidates = make([]Candidate, 0)

func DEBUG_PRINT(format string, v ...interface{}) {
    _, fname, line, _ := runtime.Caller(1)
    fmt.Printf("(%s,%d) ", fname, line); fmt.Printf(format, v...)
}

func DEBUG_PRINT_0(format string, v ...interface{}) {
    fmt.Printf(format, v...)
}

func break_word(word string, morphemes []string) {

    //DEBUG_PRINT("morphemes: ")
    //for i, val := range(morphemes) {
    //    fmt.Printf("%d %s ", i, val)
    //}
    //DEBUG_PRINT("\n")

    if word == "" {
        // DEBUG_PRINT("word: ''\n")
        if len(morphemes) > 0 {
            // DEBUG_PRINT("-->candidates = append()\n")
            candidate := Candidate{morphemes, 0}
            candidates = append(candidates, candidate)
        }
        return
    }

    // DEBUG_PRINT("--n: %d, %s\n", len(word), word)

    for i, n := 1, len(word); i < n+1; i++ {
        prefix := word[0:i]
        // DEBUG_PRINT("word:%s, prefix:%s, i=%d, len(word):%d ==> ", word, prefix, i, len(word))

        if _, ok := dict[prefix]; ok {
            tail := word[i: ]
            // DEBUG_PRINT_0(" tail %s, call break_word(%s)\n", tail, tail)

            morphemes2 := append(morphemes, prefix)

            break_word(tail, morphemes2)
        } else {
            // DEBUG_PRINT_0("prefix:%s not found in dict\n", prefix)
        }
    }
}

func MorphInit(fileName string) {
    file, err := os.Open(fileName)
    if err != nil {
        log.Fatal(err)
    }
    defer file.Close()

    reader := bufio.NewReader(file)
    for {
        line, err := reader.ReadString('\n')

        if err == io.EOF {
            break
        } else if err != nil {
            log.Fatal(err)
            return
        }

        // --------------------------------------------
        // line format
        // "GX,-a-", "GX,a-", "GX,a", "GX,a!", "GX,-a"
        // "GX,-ad/o", "GX,-igx/i"
        // --------------------------------------------
        row := strings.Split(strings.TrimSpace(line), ",")
        if len(row) < 2 {
            fmt.Println("Ignored line: ", line)
            continue
        }

        level, entry := row[0], row[1]
        root, prefix, stem, postfix, org := "", "", "", "", entry

        if len(entry) >= 2 {
            if entry[0:1] == "-" {
                prefix = "-"
                entry = entry[1:]
            }
        }


        // DEBUG_PRINT("-\n");

        if len(entry) >= 3 {
            if entry[len(entry)-2:len(entry)-1] == "/" {
                postfix = entry[len(entry)-1:len(entry)]
                entry = entry[0:len(entry)-2]
            }
        }

        // DEBUG_PRINT("-\n");

        stem = entry
        root = strings.ToLower(stem)

        if _, ok := dict[root]; ok {
            dict[root].org += "|" + org
        } else {
            dict[root] = &MInfo{ level, root, prefix, stem, postfix, org }
        }

        // fmt.Println("LOADED: ", root, dict[root])
    }

    // for root, MInfo := range(dict) {
    //     fmt.Println(root, " ", MInfo)
    // }
}

func weight_and_sort() {
    for i := 0; i < len(candidates); i++ {
        n := len(candidates[i].morphs)
        candidates[i].weight += n*3 // the longer the less preferred

        for j := 0; j < n; j++ {
            if j < (n-1) {
                dict_entry1,ok1 := dict[candidates[i].morphs[j]]
                dict_entry2,ok2 := dict[candidates[i].morphs[j+1]]

                if (ok1 && dict_entry1.postfix == "") {
                    candidates[i].weight += 3 // not prefereble if ending morph comes before other
                }

                if (ok1 && dict_entry1.postfix == "-") {
                    candidates[i].weight += -1
                }

                if (ok1 && dict_entry1.postfix == "!") {
                    candidates[i].weight += +10  // no! it's a word with '!', that doesn't allow others come after
                }

                if (ok2 && dict_entry2.prefix == "") {
                    candidates[i].weight += 1 // not prefereble if begining morph comes after other
                }

                if (ok2 && dict_entry2.prefix == "-") {
                    candidates[i].weight -= 3 // not prefereble if begining morph comes after other
                }

                if ! ok1 {
                    candidates[i].weight += 10
                }
                if ! ok2 {
                    candidates[i].weight += 10
                }

                if ok2 && dict_entry2.prefix == "" && dict_entry2.postfix == "-" {
                    candidates[i].weight += 3
                }

                if ok1 && ok2 { // condidering Esperanto word formation
                    // if entry1 is a verb, then entry2 should be verbal or adverbal
                    if dict_entry1.postfix == "i" || strings.Index(dict_entry1.org, "/i,") > 0  {

                        verb_endings := map[string]bool {
                                "i": true, "it": true, "at": true, "ut": true,
                                "is": true, "ig": true, "iĝ": true, "as": true,
                                "us": true, "u": true, "ant": true, "ont": true,
                                "int": true, "ind": true, "end": true }

                        if _, ok := verb_endings[dict_entry2.stem]; ok && dict_entry2.prefix == "-" {
                            candidates[i].weight -= 4
                            break
                        }

                        if dict_entry2.prefix == "-" && dict_entry2.stem == "o" {
                            candidates[i].weight += 7
                        }

                        if dict_entry2.prefix == "-" && dict_entry2.stem == "e" {
                            candidates[i].weight -= 2
                        }
                    }

                    // if entry1 is a noun, then entry2 should be nounal ending
                    if dict_entry1.postfix == "o" || strings.Index(dict_entry1.org, "/o,") > 0  {
                        if dict_entry2.prefix == "-" && dict_entry2.stem == "o" {
                            candidates[i].weight -= 3
                            break
                        }

                        if dict_entry2.prefix == "-" && dict_entry2.stem == "a" {
                            candidates[i].weight -= 2
                        }

                        verb_endings := map[string]bool {
                                "i": true, "it": true, "at": true, "ut": true,
                                "is": true, "ig": true, "iĝ": true, "as": true,
                                "us": true, "u": true, "ant": true, "ont": true,
                                "int": true, "ind": true, "end": true }

                        if _, ok := verb_endings[dict_entry2.stem]; ok && dict_entry2.prefix == "-" {
                            candidates[i].weight += 7
                            break
                        }

                    }

                    // if entry1 is an adjective, then entry2 is preferred to be 'a'
                    if dict_entry1.postfix == "a" || strings.Index(dict_entry1.org, "/a,") > 0  {
                        if dict_entry2.prefix == "-" && dict_entry2.stem == "a" {
                            candidates[i].weight -= 3
                        }
                    }

                    // if entry1 is an adverb, then entry2 is preferred to be 'e'
                    if dict_entry1.postfix == "e" || strings.Index(dict_entry1.org, "/e,") > 0  {
                        if dict_entry2.prefix == "-" && dict_entry2.stem == "e" {
                            candidates[i].weight -= 3
                        }
                    }
                }

                if ok1 && ok2 { // condidering Esperanto word formation
                    if dict_entry2.prefix == "-" && (dict_entry2.stem == "op" || dict_entry2.stem == "on") {
                        if dict_entry1.postfix == "" { // numerals
                            candidates[i].weight -= 3
                        } else {
                            candidates[i].weight += 1
                        }
                    }
                }
            }

        }

        for j := 0; j < n; j++ {
            // prefer longer words
            dict_entryJ, okJ := dict[candidates[i].morphs[j]]

            if okJ && dict_entryJ.prefix == "-" && dict_entryJ.postfix == "-" {
                continue
            }

            if okJ && dict_entryJ.prefix == "-" && dict_entryJ.postfix == "" {
                continue
            }

            if okJ && dict_entryJ.prefix == "" && dict_entryJ.postfix == "" {
                continue
            }

            if len(candidates[i].morphs[j]) == 3 {
                candidates[i].weight += -3
            } else if len(candidates[i].morphs[j]) == 4 {
                candidates[i].weight += -7
            } else if len(candidates[i].morphs[j]) == 5 {
                candidates[i].weight += -10
            } else if len(candidates[i].morphs[j]) > 5 {
                candidates[i].weight += -15
            }
        }

        dict_entry0, ok0 := dict[candidates[i].morphs[0]]
        if ok0 && dict_entry0.prefix != "" {
            candidates[i].weight += +9
        }

        dict_entryN, okN := dict[candidates[i].morphs[n-1]]
        if okN && dict_entryN.postfix == "" {
            candidates[i].weight -= 3 // preferable, ending word comes last
        }
        if okN && dict_entryN.postfix == "-" {
            candidates[i].weight += +7 // not preferable, if non-ending prefix comes last
        }
        if okN && dict_entryN.postfix == "!" {
            candidates[i].weight += -11 // preferable, ending as a exclamation word
        }


        for j := 1; j < (n-1); j++ {
            dict_entryJ, okJ := dict[candidates[i].morphs[j]]
            if okJ && dict_entryJ.stem == "i" {
                candidates[i].weight += +9
            }
        }
    }

    sort.Sort(ByWeight(candidates))
}

func split_ending(word string) {
    endings := []string{ "as", "is", "os", "us", "ajn", "ojn", "aj", "oj", "an", "on", "a", "i", "o", "e", "u" }
    for _, ending := range(endings) {
        if strings.HasSuffix(word, ending) {
            morphemes := make([]string, 0)
            morphemes = append(morphemes, strings.TrimSuffix(word, ending))
            morphemes = append(morphemes, ending)
            candidate := Candidate{morphemes, 0}
            candidates = append(candidates, candidate)
            break
        }
    }
}

func MorphAnalysis(word string) []Candidate {

    candidates = make([]Candidate, 0)

    word = strings.ToLower(word)

    break_word(word, make([]string, 0))
    if len(candidates) == 0 {
        split_ending(word)
    }

    weight_and_sort()

    return candidates
}


func MorphSuperSigno(word string) string {
    super_signs := map[string]string {
          "AUX": "AŬ", "EUX": "EŬ", "Aux": "Aŭ", "Eux": "Eŭ",
          "aux": "aŭ", "eux": "eŭ", "au^": "aŭ", "eu^": "eŭ",
          "auh": "aŭ", "euh": "eŭ",

          "UX": "Ŭ", "Ux": "Ŭ", "ux": "ŭ", "u^": "ŭ", "U^": "Ŭ",

          "CX": "Ĉ", "Cx": "Ĉ", "cx": "ĉ", "CH": "Ĉ",
          "Ch": "Ĉ", "ch": "ĉ", "C^": "Ĉ", "c^": "ĉ",

          "GX": "Ĝ", "Gx": "Ĝ", "gx": "ĝ", "GH": "Ĝ",
          "Gh": "Ĝ", "gh": "ĝ", "G^": "Ĝ", "g^": "ĝ",

          "HX": "Ĥ", "Hx": "Ĥ", "hx": "ĥ", "HH": "Ĥ",
          "Hh": "Ĥ", "hh": "ĥ", "H^": "Ĥ", "h^": "ĥ",

          "JX": "Ĵ", "Jx": "Ĵ", "jx": "ĵ", "JH": "Ĵ",
          "Jh": "Ĵ", "jh": "ĵ", "J^": "Ĵ", "j^": "ĵ",

          "SX": "Ŝ", "Sx": "Ŝ", "sx": "ŝ", "SH": "Ŝ",
          "Sh": "Ŝ", "sh": "ŝ", "S^": "Ŝ", "s^": "ŝ",
        }

    super_signs2 := map[string]string {
        "AU": "AŬ", "EU": "AŬ", "au": "aŭ", "eu": "eŭ",
    }

    for {
        found := false
        for ch1, ch2 := range(super_signs) {
            if strings.Contains(word, ch1) {
                word = strings.Replace(word, ch1, ch2, -1)
                found = true
            }
        }
        for ch1, ch2 := range(super_signs2) {
            if strings.Contains(word, ch1) {
                word = strings.Replace(word, ch1, ch2, -1)
                found = true
            }
        }
        if ! found {
            break
        }

    }

    return word
}

func MorphCleanUp(word string) string {
    special_chars := []string{ "~", "`", "!", "@", "#", "$", "%",
        "^", "&", "*", "(", ")", "-", "_", "+", "=", "{", "}", "[",
        "]", "|", "\\", ":", ";", "\"", "'", "<", ">", "?", ",", ".", "/",
        "“", "”", "«", "»", "‘", "’", "’", "‘", "『", "』", "「", "」", "▶",
        }

    for {
        found := false
        for _, special_char := range(special_chars) {
            if strings.HasPrefix(word, special_char) {
                word = strings.TrimPrefix(word, special_char)
                found = true
            }
            if strings.HasSuffix(word, special_char) {
                word = strings.TrimSuffix(word, special_char)
                found = true
            }
        }

        if ! found {
            break
        }
    }

    return word
}
