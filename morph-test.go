package main

import (
    "os"
    "fmt"
    "runtime"
    "./morph"
)

func DEBUG_PRINT(format string, v ...interface{}) {
    _, fname, line, _ := runtime.Caller(1)
    fmt.Printf("(%s,%d) ", fname, line); fmt.Printf(format, v...)
}

func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s 'word'\n", os.Args[0])
        os.Exit(0)
    }

    word := os.Args[1]

    morph.MorphInit("./morph/BRO.dic")

    DEBUG_PRINT("word: %s\n", word)

    word = morph.MorphCleanUp(word)

    DEBUG_PRINT("word: %s\n", word)

    word = morph.MorphSuperSigno(word)

    DEBUG_PRINT("word: %s\n", word)

    candidates := morph.MorphAnalysis(word)

    for _, candidate := range(candidates) {
        fmt.Println(candidate.Morphs(), candidate.Weight())
    }
}
