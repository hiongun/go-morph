package main

import (
    "bytes"
    "fmt"
    "io"
    "io/ioutil"
    "mime/multipart"
    "net/http"
    "os"
    "runtime"
)

func DEBUG_PRINT(format string, v ...interface{}) {
    _, fname, line, _ := runtime.Caller(1)
    fmt.Printf("(%s,%d) ", fname, line)
    fmt.Printf(format, v...)
}

func postFile(filename string, targetUrl string) error {
    bodyBuf := &bytes.Buffer{}
    bodyWriter := multipart.NewWriter(bodyBuf)

    // this step is very important
    fileWriter, err := bodyWriter.CreateFormFile("uploadfile", filename)
    if err != nil {
        fmt.Println("error writing to buffer")
        return err
    }

    // open file handle
    fh, err := os.Open(filename)
    if err != nil {
        fmt.Println("error opening file")
        return err
    }

    //iocopy
    _, err = io.Copy(fileWriter, fh)
    if err != nil {
        return err
    }

    contentType := bodyWriter.FormDataContentType()
    bodyWriter.Close()

    DEBUG_PRINT("http.Post(%s)\n", targetUrl)

    resp, err := http.Post(targetUrl, contentType, bodyBuf)
    if err != nil {
        return err
    }
    defer resp.Body.Close()
    resp_body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        return err
    }
    fmt.Println(resp.Status)
    fmt.Println(string(resp_body))
    return nil
}

// sample usage
func main() {
    if len(os.Args) < 2 {
        fmt.Printf("Usage: %s URL file\n", os.Args[0])
        os.Exit(0)
    }

    target_url := os.Args[1]
    filename := os.Args[2]

    DEBUG_PRINT("target_url: %s\n", target_url)
    DEBUG_PRINT("filename: %s\n", filename)

    postFile(filename, target_url)
}
