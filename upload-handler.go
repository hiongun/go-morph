package main

import (
    "net/http"
    "fmt"
    "os"
    "io"
    "io/ioutil"
    "runtime"
    "os/exec"
    "strings"
    "strconv"
    "./morph"
    "sort"
)

func DEBUG_PRINT(format string, v ...interface{}) {
    _, fname, line, _ := runtime.Caller(1)
    fmt.Printf("(%s,%d) ", fname, line)
    fmt.Printf(format, v...)
}

func load_page(fname string) string {
    buf, err := ioutil.ReadFile(fname)
    if err != nil {
        return ""
    }
    return string(buf)
}

func upload1_form(w http.ResponseWriter, r *http.Request) {
    html := load_page("./pages/page1.html")
    w.Header().Set("Content-Type", "text/html")
    w.Write([]byte(html))
}

func upload2_form(w http.ResponseWriter, r *http.Request) {
    html := load_page("./pages/page2.html")
    w.Header().Set("Content-Type", "text/html")
    w.Write([]byte(html))
}


func upload3_form(w http.ResponseWriter, r *http.Request) {
    html := load_page("./pages/page3.html")
    w.Header().Set("Content-Type", "text/html")
    w.Write([]byte(html))
}


func upload1(w http.ResponseWriter, r *http.Request) {
    // DEBUG_PRINT("method: %s\n", r.Method)

    r.ParseMultipartForm(32 << 20)
    file, handler, err := r.FormFile("uploadfile")
    if err != nil {
        fmt.Println(err)
        return
    }
    defer file.Close()

    // DEBUG_PRINT("filename: %s\n", handler.Filename)

    f, err := os.OpenFile("./data/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
    if err != nil {
        fmt.Println(err)
        return
    }
    defer f.Close()
    io.Copy(f, file)

    cmd := exec.Command("./sn3f_exe", "-U", "./data/" + handler.Filename)
    stdout, err := cmd.Output()

    w.Header().Set("Content-Type", "text/html; charset=utf-8")

    html := load_page("./pages/page1.html")
    w.Write([]byte(html))

    w.Write([]byte(string("<HTML><table border=1 width=100% height=100%><tr><td width=100% height=100%>")))

    w.Write([]byte(string("<textarea style='width:100%; height:100%'>")))

    w.Write([]byte(string(stdout)))

    w.Write([]byte(string("</textarea></td></tr></tables><HTML>")))
}

func upload2(w http.ResponseWriter, r *http.Request) {
    // DEBUG_PRINT("method: %s\n", r.Method)

    r.ParseMultipartForm(32 << 20)
    file, handler, err := r.FormFile("uploadfile")
    if err != nil {
        fmt.Println(err)
        return
    }
    defer file.Close()

    // DEBUG_PRINT("filename: %s\n", handler.Filename)

    f, err := os.OpenFile("./data/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
    if err != nil {
        fmt.Println(err)
        return
    }
    defer f.Close()
    io.Copy(f, file)

    cmd := exec.Command("./sn3f_exe", "-U", "./data/" + handler.Filename)
    stdout, err := cmd.Output()

    words := strings.Fields(string(stdout))

    output := ""

    for i := 0; i < len(words); i++ {
        word := words[i]
        if strings.Index(word, "(") >= 0 {
            idx := strings.Index(word, "(")
            word1 := word[0:idx]
            word2 := word[idx+1: ]
            words[i] = word1

            // words.InsertAfter(i, word2)
            words = append(words[:i], append([]string{word2}, words[i:]...)...)
            i -= 1
            continue
        }

        if strings.Index(word, ")") >= 0 {
            idx := strings.Index(word, ")")
            word1 := word[0:idx]
            word2 := word[idx+1: ]
            words[i] = word1

            words = append(words[:i], append([]string{word2}, words[i:]...)...) // words.InsertAfter(i, word2)
            i -= 1
            continue
        }

        if strings.Index(word, ",") >= 0 {
            idx := strings.Index(word, ",")
            word1 := word[0:idx]
            word2 := word[idx+1: ]
            words[i] = word1

            words = append(words[:i], append([]string{word2}, words[i:]...)...) // words.InsertAfter(i, word2)
            i -= 1
            continue
        }
    }

    dict := morph.GetDict()

    stats := map[string]int { }

    for _, word := range(words) {

        word = morph.MorphCleanUp(word)
        word = morph.MorphSuperSigno(word)

        if len(word) == 0 {
            continue
        }

        candidates := morph.MorphAnalysis(word)

        if len(candidates) > 0 {
            morphs := candidates[0].Morphs()

            output += "<br>"

            for i, morph := range(morphs) {
                output += string(morph)

                if i < (len(morphs)-1) {
                    output += "/"
                }

                if _, ok := dict[morph]; ok {
                    if _, ok := stats[morph]; ok {
                        stats[morph] += 1
                    } else {
                        stats[morph] = 1
                    }
                }
            }
        } else {
            output += "<br>" + word

            stats[word] += 1
        }
    }

    lines := make([]string, 0)
    for key, val := range(stats) {
        line := fmt.Sprintf("%05d,%s", val, key)
        lines = append(lines, line)
    }

    sort.Sort(sort.Reverse(sort.StringSlice(lines)))

    output = ""

    level_counts := map[string]int {}


    // 사전에 있는 단어 우선 출력
    for _, line := range(lines) {
        // output += line + "<br>"
        key_val := strings.Split(line, ",")
        num := key_val[0]
        morph := key_val[1]

        if minfo, ok := dict[morph]; ok {
            output += num + ", " + morph + ", " + minfo.Level() + ", " + minfo.Org() + "\n"
            level_counts[minfo.Level()] += 1
        }
    }

    // 사전에 없는 단어 뒤에 출력
    for _, line := range(lines) {
        // output += line + "<br>"
        key_val := strings.Split(line, ",")
        num := key_val[0]
        morph := key_val[1]

        if _, ok := dict[morph]; ! ok {
            output += num + ", " + morph + ", __, __\n"
            level_counts["GX"] += 1
        }
    }

    output += "------------------\nNo of distinct elements:\n"
    groups := []string{"G1", "G2", "G3", "G4", "G5", "G6", "G7", "G8", "G9", "GA", "GB", "GX"}
    for _, Gi := range(groups) {
        output += Gi + ": " + strconv.Itoa(level_counts[Gi]) + "\n"
    }

    w.Header().Set("Content-Type", "text/html; charset=utf-8")

    html := load_page("./pages/page2.html")
    w.Write([]byte(html))

    w.Write([]byte(string("<HTML>")))
    w.Write([]byte(string("<table border=1 width=100% height=100%><tr><td width=100% height=100%>")))

    // w.Write([]byte(string("<textarea style='width:100%; height:100%'>")))
    w.Write([]byte(string("<pre>")))

    w.Write([]byte(string(output)))

    w.Write([]byte(string("</pre>")))
    // w.Write([]byte(string("</textarea></td></tr></tables>")))
    w.Write([]byte(string("<HTML>")))
}

func upload3(w http.ResponseWriter, r *http.Request) {
    // DEBUG_PRINT("method: %s\n", r.Method)

    r.ParseMultipartForm(32 << 20)
    file, handler, err := r.FormFile("uploadfile")
    if err != nil {
        fmt.Println(err)
        return
    }
    defer file.Close()

    // DEBUG_PRINT("filename: %s\n", handler.Filename)

    f, err := os.OpenFile("./data/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
    if err != nil {
        fmt.Println(err)
        return
    }
    defer f.Close()
    io.Copy(f, file)

    cmd := exec.Command("./sn3f_exe", "-U", "./data/" + handler.Filename)
    stdout, err := cmd.Output()

    words := strings.Fields(string(stdout))

    output := ""

    for i := 0; i < len(words); i++ {
        word := words[i]
        if strings.Index(word, "(") >= 0 {
            idx := strings.Index(word, "(")
            word1 := word[0:idx]
            word2 := word[idx+1: ]
            words[i] = word1

            // words.InsertAfter(i, word2)
            words = append(words[:i], append([]string{word2}, words[i:]...)...)
            i -= 1
            continue

        }

        if strings.Index(word, ")") >= 0 {
            idx := strings.Index(word, ")")
            word1 := word[0:idx]
            word2 := word[idx+1: ]
            words[i] = word1

            words = append(words[:i], append([]string{word2}, words[i:]...)...) // words.InsertAfter(i, word2)
            i -= 1
            continue
        }

        if strings.Index(word, ",") >= 0 {
            idx := strings.Index(word, ",")
            word1 := word[0:idx]
            word2 := word[idx+1: ]
            words[i] = word1

            words = append(words[:i], append([]string{word2}, words[i:]...)...) // words.InsertAfter(i, word2)
            i -= 1
            continue
        }
    }

    for _, word := range(words) {

        word = morph.MorphCleanUp(word)
        word = morph.MorphSuperSigno(word)

        if len(word) == 0 {
            continue
        }

        candidates := morph.MorphAnalysis(word)

        if len(candidates) > 0 {
            morphs := candidates[0].Morphs()

            output += " "

            for i, morph := range(morphs) {
                output += string(morph)
                if i < (len(morphs)-1) {
                    output += "/"
                }
            }
        } else {
            output += " " + word
        }
    }

    w.Header().Set("Content-Type", "text/html; charset=utf-8")

    html := load_page("./pages/page3.html")
    w.Write([]byte(html))

    w.Write([]byte(string("<HTML>")))
    w.Write([]byte(string("<table border=1 width=100% height=100%><tr><td width=100% height=100%>")))

    w.Write([]byte(string("<textarea style='width:100%; height:100%'>")))

    w.Write([]byte(string(output)))

    w.Write([]byte(string("</textarea></td></tr></tables>")))
    w.Write([]byte(string("<HTML>")))
}

func main() {
    morph.MorphInit("./morph/BRO.dic")

    mux := http.NewServeMux()
    mux.HandleFunc("/upload1", upload1)
    mux.HandleFunc("/upload2", upload2)
    mux.HandleFunc("/upload3", upload3)
    mux.HandleFunc("/conv2txt", upload1_form)
    mux.HandleFunc("/radikoj", upload2_form)
    mux.HandleFunc("/morphs", upload3_form)
    http.ListenAndServe(":9010", mux)
}
